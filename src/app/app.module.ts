import { HttpClientModule, HttpClient } from '@angular/common/http';
import { isDevMode, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { WebsiteModule } from './website-section/website.module';
import { ABONavigatorService } from './shared/services/abo-navigator.service';
import { AuthenticationService } from './shared/services/authentication.service';
import { SecurityGuardService } from './security-section/shared/services/security-guard.service';
import { JWTTokenService } from './shared/services/models/jwt-token.service';
import { UserService } from './shared/services/models/user.service';
import { ABOTranslationLoaderService } from './shared/services/abo-translation-loader.service';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { AdminGuardService } from './admin-section/shared/services/admin-guard.service';
import { DevToolsExtension, NgRedux, NgReduxModule } from '@angular-redux/store';
import { INIT_STATE, State } from './shared/redux/models/state';
import { rootReducer } from './shared/redux/store';
import { ShopModule } from './shop-section/shop.module';

export function httpAppLoaderFactory( http: HttpClient ) {
    return new TranslateHttpLoader( http, './assets/i18n/', '/app.min.json' );
}

@NgModule( {
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        NgReduxModule,
        WebsiteModule,
        ShopModule,
        AppRoutingModule,
        MDBBootstrapModule.forRoot(),
        TranslateModule.forRoot( {
            loader: {
                provide: TranslateLoader,
                useFactory: httpAppLoaderFactory,
                deps: [ HttpClient ],
            },
        } ),
    ],
    providers: [
        ABONavigatorService,
        AuthGuardService,
        AuthenticationService,
        AdminGuardService,
        SecurityGuardService,
        JWTTokenService,
        UserService,
        ABOTranslationLoaderService,
    ],
    schemas: [ NO_ERRORS_SCHEMA ],
    bootstrap: [ AppComponent ],
} )
export class AppModule {

    constructor( ngRedux: NgRedux<State>, devTools: DevToolsExtension, translate: TranslateService ) {

        const storeEnhancers = devTools.isEnabled() && isDevMode() ? [ devTools.enhancer() ] : [];
        ngRedux.configureStore( rootReducer, INIT_STATE, [], storeEnhancers );

        translate.addLangs( [ 'ar', 'en', 'fr' ] );
        translate.setDefaultLang( 'en' );
        translate.use( 'en' );
    }
}
