import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component( {
    templateUrl: './products-collection.component.html',
    styleUrls  : [ './products-collection.component.min.css' ],
} )
export class ProductsCollectionComponent implements OnInit {

    constructor( private meta: Meta,
        private title: Title ) {

    }

    ngOnInit() {

        this.title.setTitle( 'My spiffy profile page' );
        this.meta.addTags( [
            { name: 'author', content: 'Aymen Ben Othmen' },
            { name: 'keywords', content: 'angular 4 tutorial, angular seo' },
            { name: 'description', content: 'This is my profile page description!!' },
        ] );
    }
}
