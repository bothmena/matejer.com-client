import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MatejerCommonModule } from '../shared/modules/matejer-common.module';

import { ShopRoutingModule } from './shop-routing.module';
import { ShopComponent } from './shop/shop.component';
import { ProductsCategoryComponent } from './products-category/products-category.component';
import { ProductsCollectionComponent } from './products-collection/products-collection.component';
import { ProductsComponent } from './products/products.component';
import { SearchComponent } from './search/search.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule( {
    imports: [
        MatejerCommonModule,
        TranslateModule,
        ShopRoutingModule,
    ],
    declarations: [
        ShopComponent,
        ProfileComponent,
        SearchComponent,
        ProductsComponent,
        ProductsCollectionComponent,
        ProductsCategoryComponent,
    ],
    providers: [],
} )
export class ShopModule {
}
