import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShopComponent } from './shop/shop.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchComponent } from './search/search.component';
import { ProductsComponent } from './products/products.component';
import { ProductsCollectionComponent } from './products-collection/products-collection.component';
import { ProductsCategoryComponent } from './products-category/products-category.component';

const routes: Routes = [
    {
        path            : ':shop_slug',
        component       : ShopComponent,
        children        : [
            { path: '', component: ProfileComponent },                                  // s_prf
            { path: 'search', component: SearchComponent },                             // s_src
            { path: 'products', component: ProductsComponent },                         // s_prd
            { path: 'products/l/:collection', component: ProductsCollectionComponent }, // s_prl
            { path: 'products/c/:category', component: ProductsCategoryComponent },     // s_prc
        ],
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ],
} )
export class ShopRoutingModule {
}
