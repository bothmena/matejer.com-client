import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component( {
    templateUrl: './profile.component.html',
    styleUrls  : [ './profile.component.min.css' ],
} )
export class ProfileComponent implements OnInit {

    constructor( private meta: Meta,
        private title: Title ) {

    }

    ngOnInit() {

        this.title.setTitle( 'My spiffy profile page' );
        this.meta.addTags( [
            { name: 'author', content: 'Aymen Ben Othmen' },
            { name: 'keywords', content: 'angular 4 tutorial, angular seo' },
            { name: 'description', content: 'This is my profile page description!!' },
        ] );
    }
}
