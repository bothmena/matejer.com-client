import { HttpResponse } from '@angular/common/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

/**
 * Created by bothmena on 22/01/17.
 */

export class ABOHttp {

    public static handleError( error: HttpResponse<any> | any ) {

        // In a real world app, we might use a remote logging infrastructure
        if ( error instanceof HttpResponse ) {

            return Observable.throw( error || '' );
        } else {

            const errMsg = error.message ? error.message : error.toString();
            return Observable.throw( errMsg );
        }
    }
}
