/**
 * Created by bothmena on 19/01/17.
 */

import { NgRedux } from '@angular-redux/store';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { JWTToken } from '../models/jwt-token';
import { FireData } from '../redux/models/firebase-state';
import { State } from '../redux/models/state';
import { AppGlobals } from '../utils/app.globals';
import { ABOHttp } from './abo-http';
import { ABONavigatorService } from './abo-navigator.service';
import { JWTTokenService } from './models/jwt-token.service';

@Injectable()
export class AuthenticationService extends ABOHttp {

    private _authenticateUrl: string = AppGlobals.BASE_API_URL + AppGlobals.AUTHENTICATE_URL;
    private _refreshTokenUrl: string = AppGlobals.BASE_API_URL + AppGlobals.REFRESH_TOKEN_URL;
    private _rememberMe: boolean;
    private _redirectUrl: string;
    private subscription: Subscription;

    private static extractToken( res: HttpResponse<any>|any ): JWTToken {

        const body: any = res;
        if ( body.token && body.refresh_token ) {
            return new JWTToken( body.token, body.refresh_token );
        }
        return null;
    }

    constructor( private _http: HttpClient,
        private _navigator: ABONavigatorService,
        private tokenService: JWTTokenService,
        private ngRedux: NgRedux<State>,
        private fauth: AngularFireAuth ) {

        super();
        this.checkToken();
        setInterval( () => this.checkToken(), 30000 );
    }

    private checkToken() {

        if ( this.tokenService.isAuthenticated() && this.tokenService.isTokenExpired( 121 ) ) {

            this.refreshToken().toPromise()
                .then( jwt => {
                    this.tokenService.jwtToken = jwt;
                    this.saveJWTToken();
                } );
        }
    }

    public isLoggedIn(): boolean {

        return this.tokenService.isAuthenticated();
    }

    public logout(): any { // returns firebase.Promise<any>

        this.tokenService.jwtToken = null;
        this.saveJWTToken();
        return this.fauth.auth.signOut();
    }

    public authenticate( login: { password: string, rememberMe: boolean, username: string } ): Observable<JWTToken> {

        this._rememberMe = login.rememberMe;
        const data = JSON.stringify( { '_username': login.username, '_password': login.password } );

        return this._http.post( this._authenticateUrl, data, {
            headers: new HttpHeaders().set( 'content-type', 'application/json' ),
        } ).map( res => AuthenticationService.extractToken( res ) );
        // .catch( AuthenticationService.handleError );
    }

    public onAuthenticateSuccess( jwtToken: JWTToken, redirect: boolean = true ) {

        this.tokenService.jwtToken = jwtToken;
        this.saveJWTToken();
        if ( redirect ) {
            this.redirect();
        }
    }

    public onAuthenticateFailure( error: string | any ) {

        /**
         * @todo should I remove the token and clear the storage even if there is no
         * connection error or any other error not related to the authentication?
         */
        this.tokenService.jwtToken = null;
        localStorage.clear();

        console.log( error );
    }

    public goToProfile() {

        if ( this.tokenService.username === '' ) {

            this._navigator.goto( 'c_lgn' );
            return;
        }

        switch ( this.tokenService.role ) {

            case 'student':
                this._navigator.goto( 's_prf', this.tokenService.username );
                break;
            case 'professor':
                this._navigator.goto( 'p_prf', this.tokenService.username );
                break;
            case 'parent':
                this._navigator.goto( 'r_prf', this.tokenService.username );
                break;
            case 'ins_staff':
                this.subscription = this.ngRedux.select( [ 'firebase', 'data' ] ).subscribe(
                    ( data: FireData ) => {
                        const slug = data.institute_slug;
                        if ( slug != null && slug !== '' ) {
                            this._navigator.goto( 'i_mprf', slug, this.tokenService.username );
                            this.subscription.unsubscribe();
                        }
                    },
                );
                break;
            case 'ins_admin':
                this.subscription = this.ngRedux.select( [ 'firebase', 'data' ] ).subscribe(
                    ( data: FireData ) => {
                        if ( data && data.institute_slug != null && data.institute_slug !== '' ) {
                            this._navigator.goto( 'i_mprf', data.institute_slug,
                                this.tokenService.username );
                            this.subscription.unsubscribe();
                        }
                    },
                );
                break;
            case 'admin':
                this._navigator.goto( 'a_dsh', this.tokenService.username );
                break;
            default:
                this._navigator.goto( 'w_hm' );
                break;
        }
    }

    private refreshToken(): Observable<JWTToken> {

        const data = JSON.stringify( { 'refresh_token': this.tokenService.jwtToken.refreshToken } );

        return this._http.post( this._refreshTokenUrl, data, {
            headers: new HttpHeaders().set( 'content-type', 'application/json' ),
        } ).map( res => AuthenticationService.extractToken( res ) )
            .catch( AuthenticationService.handleError );
    }

    private saveJWTToken() {

        if ( this.tokenService.jwtToken === null ) {

            for ( const key of [ 'token', 'refresh_token', 'username', 'role', 'uid' ] ) {
                localStorage.removeItem( key );
            }
        } else {

            localStorage.setItem( 'token', this.tokenService.jwtToken.token );
            localStorage.setItem( 'refresh_token', this.tokenService.jwtToken.refreshToken );
            localStorage.setItem( 'username', this.tokenService.username );
            localStorage.setItem( 'role', this.tokenService.role );
            localStorage.setItem( 'uid', this.tokenService.uid );
        }
    }

    private redirect(): void {

        this.goToProfile();
    }

    set redirectUrl( value: string ) {
        this._redirectUrl = value;
    }
}
