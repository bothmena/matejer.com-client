import { TestBed, inject } from '@angular/core/testing';

import { DepartmentsResolverService } from '../../../../../../houcine/src/app/shared/services/resolvers/departments-resolver.service';

describe('DepartmentsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DepartmentsResolverService]
    });
  });

  it('should be created', inject([DepartmentsResolverService], (service: DepartmentsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
