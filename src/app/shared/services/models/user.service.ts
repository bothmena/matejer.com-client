import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { User } from '../../models/user';
import { AppGlobals } from '../../utils/app.globals';
import { ABOHttp } from '../abo-http';
import { ABONavigatorService } from '../abo-navigator.service';
import { JWTTokenService } from './jwt-token.service';

@Injectable()
export class UserService extends ABOHttp {

    private usersUrl = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/users';

    static extractUsers( res: HttpResponse<any>|any ): User[] {

        const users: User[] = [];
        for ( const user of res ) {

            users.push( User.build( user ) );
        }
        return users;
    }

    public static extractUser( res: HttpResponse<any>|any ): User {

        return User.build( res );
    }

    constructor( private http: HttpClient,
        private translate: TranslateService,
        private jwtService: JWTTokenService,
        private fauth: AngularFireAuth,
        private fdb: AngularFireDatabase,
        private navigator: ABONavigatorService ) {
        super();
    }

    getUsers( options: {} = {} ): Observable<User[]> {

        let url = this.usersUrl;
        let i = 0;
        for (const option in options) {
            if (options.hasOwnProperty(option)) {

                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[ option ];
            }
        }
        return this.http.get( url )
            .map( UserService.extractUsers )
            .catch( ABOHttp.handleError );
    }

    getUser( username: string, options: {} = {} ): Observable<User> {

        let url = this.usersUrl + '/' + username;
        let i = 0;
        for (const option in options) {
            if (options.hasOwnProperty(option)) {

                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[ option ];
            }
        }
        return this.http.get( url )
            .map( UserService.extractUser )
            .catch( ABOHttp.handleError );
    }

    getUserExistByToken( token: string, enabled?: boolean ): Observable<boolean> {

        let url = this.usersUrl + '/t/' + token;
        if ( enabled !== null ) {

            url += enabled ? '?enabled=true' : '?enabled=false';
        }
        return this.http.get( url )
            .map( ( res: HttpResponse<any> ) => {

                const body: any = res;
                if ( !body.exist ) {

                    this.translate.get( 'init_account.404', { token: token } ).subscribe(
                        ( translated ) => console.log( translated, 10000 ), // toast
                    );
                    this.navigator.goto( 'w_hm' );
                }
                return body.exist;
            } ).catch( ( error: HttpResponse<any> | any ) => {
                try {

                    console.log( error.toString() );
                } catch ( e ) {
                }
                finally {
                    this.translate.get( 'init_account.500', { token: token } ).subscribe(
                        ( translated ) => console.log( translated, 10000 ), // toast
                    );
                    this.navigator.goto( 'w_hm' );
                }
                return Observable.throw( null );
            } );
    }

    updateUserFireData() {

        if ( this.jwtService.username !== '' ) {

            this.http.get( this.usersUrl + '/' + this.jwtService.username + '/fire-data' )
                .map( ( res: HttpResponse<any> ) => res ).toPromise()
                .catch( ABOHttp.handleError )
                .then( ( data ) => {

                    this.fdb.object( '/users/' + this.fauth.auth.currentUser.uid ).set( data );
                } )
                .catch( ( error ) => {

                    console.log( error );
                } );
        }
    }

    newUser( data: any ): Observable<any> {

        return this.http.post( this.usersUrl, JSON.stringify( data ), {
            headers: new HttpHeaders().set( 'content-type', 'application/json' ),
        } ).catch( ABOHttp.handleError );
    }

    patchUser( userId: number, data: any ): Observable<any> {

        const headers = new HttpHeaders( {
            'Content-Type': 'application/json',
            'Authorization': 'Zednilma ' + this.jwtService.jwtToken.token,
        } );
        return this.http.patch( this.usersUrl + '/' + userId, JSON.stringify( data ), { headers: headers, } ).catch( ABOHttp.handleError );
    }

    resetPassword( userId: number, data: any ): Observable<any> {

        const headers = new HttpHeaders( {
            'Content-Type': 'application/json',
            'Authorization': 'Zednilma ' + this.jwtService.jwtToken.token,
        } );
        return this.http.post( this.usersUrl + '/' + userId + '/reset-password', JSON.stringify( data ), { headers: headers } )
            .catch( ABOHttp.handleError );
    }

    initUser( token: string, data: any ): Observable<any> {

        return this.http.post( this.usersUrl + '/init-account/' + token, JSON.stringify( data ), {
            headers: new HttpHeaders().set( 'content-type', 'application/json' ),
        } );
    }

    confirmationCode( data: string, action: string ): Observable<boolean> {

        if ( action === 'resend-code' || action === 'confirm-profile' ) {

            return this.http.post( this.usersUrl + '/' + action + '/' + data, null, {
                headers: new HttpHeaders().set( 'content-type', 'application/json' ),
            } ).map( (res: HttpResponse<any>) => {
                return res.ok;
            } )
                .catch( ABOHttp.handleError );
        }
    }
}
