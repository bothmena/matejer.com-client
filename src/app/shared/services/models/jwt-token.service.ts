import { NgRedux } from '@angular-redux/store';
import { Injectable } from '@angular/core';
import { JwtHelper } from 'angular2-jwt';
import { JWTToken } from '../../models/jwt-token';
import { AppActions } from '../../redux/app/app.actions';
import { State } from '../../redux/models/state';
import { getJWTToken } from '../../utils/util-functions';
/**
 * Created by bothmena on 22/01/17.
 */

@Injectable()
export class JWTTokenService {

    private _jwtToken: JWTToken;
    private _username: string;
    private _uid: string;
    private _role: string;
    private _exp: number;
    private _iat: number;

    private jwtHelper: JwtHelper = new JwtHelper();

    constructor( private redux: NgRedux<State> ) {

        this.jwtToken = getJWTToken();
    }

    public isAuthenticated(): boolean {

        return this.jwtToken !== null;
    }

    public isTokenExpired( offset?: number ): boolean {

        if ( this.isAuthenticated() ) {

            return this.jwtHelper.isTokenExpired( this.jwtToken.token, offset );
        }
        return true;
    }

    public getTokenExprDate() {

        return this.jwtHelper.getTokenExpirationDate( this.jwtToken.token );
    }

    set jwtToken( jwtToken: JWTToken ) {

        if ( jwtToken !== null && jwtToken.token !== '' ) {

            this._jwtToken = jwtToken;
            const decoded = this.jwtHelper.decodeToken( jwtToken.token );
            this._username = decoded.username;
            this._uid = decoded.uid;
            this._role = decoded.role;
            this._exp = +decoded.exp;
            this._iat = +decoded.iat;

        } else {

            this._jwtToken = null;
            this._username = '';
            this._uid = '';
            this._role = '';
            this._exp = 0;
            this._iat = 0;
        }
    }

    get jwtToken(): JWTToken {
        return this._jwtToken;
    }

    get username(): string {
        return this._username;
    }

    get uid(): string {
        return this._uid;
    }

    get role(): string {
        return this._role;
    }

    get exp(): number {
        return this._exp;
    }

    get iat(): number {
        return this._iat;
    }
}
