import { TestBed, inject } from '@angular/core/testing';

import { ABOTranslationLoaderService } from './abo-translation-loader.service';

describe('ZdlmTranslationLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ ABOTranslationLoaderService]
    });
  });

  it('should be created', inject([ ABOTranslationLoaderService], (service: ABOTranslationLoaderService) => {
    expect(service).toBeTruthy();
  }));
});
