/**
 * Created by bothmena on 10/03/17.
 */

import { NgRedux } from '@angular-redux/store';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { isArray } from 'util';
import { AppActions } from '../redux/app/app.actions';
import { State } from '../redux/models/state';

@Injectable()
export class ABONavigatorService {

    constructor( private _router: Router, private ngRedux: NgRedux<State> ) {
    }

    public goto( routeName: string, ...params: string[] ) {

        const routeParams: {} = {

            // WebsiteModule
            'w_hm': [ '/' ],
            'w_abt': [ 'about-us' ],
            'w_ctu': [ 'contact-us' ],
            'w_brw': [ 'browse' ],
            'w_brp': [ 'browse', 'products' ],
            'w_brs': [ 'browse', 'shops' ],

            // SecurityModule
            'c_lgn': [ 'login' ],
            'c_lgo': [ 'logout' ],
            'c_rpw': [ 'reset-password' ],
            'c_int': [ 'init-account', params[ 0 ] ],

            // ShopModule
            's_prf': [ params[ 0 ] ],
            's_src': [ params[ 0 ], 'search' ],
            's_prd': [ params[ 0 ], 'products' ],
            's_prl': [ params[ 0 ], 'products', 'l', params[ 1 ] ],
            's_prc': [ params[ 0 ], 'products', 'c', params[ 1 ] ],
        };

        console.log( 'navigating to ', routeName );

        if ( isArray( routeParams[ routeName ] ) ) {

            this.ngRedux.dispatch( AppActions.activeRouteChanged( routeName ) );
            this._router.navigate( routeParams[ routeName ] );
        } else {
            console.log( 'This route does not exist!', routeName );
        }
    }
}
