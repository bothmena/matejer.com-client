import { Pipe, PipeTransform } from '@angular/core';

@Pipe( {
    name: 'phoneNumber',
} )
export class PhoneNumberPipe implements PipeTransform {

    transform( value: string, phoneCCode: string ): any {

        value += '';
        if ( value && phoneCCode ) {

            value = value.replace(/\s+/g, '');
            if ( value.length === 8 ) {

                return '(' + phoneCCode.replace('00', '+') + ')' + ' ' + value.substr(0, 2) + ' ' +
                    value.substr(2, 3) + ' ' + value.substr( 5, 3 );
            } else {

                return 'Invalid Phone Number';
            }
        }
        return '';
    }
}
