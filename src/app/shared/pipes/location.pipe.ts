import { Pipe, PipeTransform } from '@angular/core';
import { Location } from '../models/location';
import { getCountryName } from '../utils/util-functions';

@Pipe( {
    name: 'location',
} )
export class LocationPipe implements PipeTransform {

    transform( value: Location ): any {

        let str = '';

        if ( value ) {

            str += value.address;
            str += ', ' + value.zipCode;
            str += ', ' + value.city;
            str += ', ' + value.state;
            str += ', ' + getCountryName( value.country );
        }
        return str;
    }

}
