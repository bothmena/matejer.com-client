import { Pipe, PipeTransform } from '@angular/core';

@Pipe( {
    name: 'keys',
} )
export class ObjectKeysPipe implements PipeTransform {

    transform( value: {}, args?: any ): any[] {

        if ( !value ) {
            return null;
        }
        return Object.keys( value );
    }
}
