import { Component } from '@angular/core';

@Component( {
    selector   : 'mtjr-footer',
    templateUrl: './footer.component.html',
    styleUrls  : [ './footer.component.min.css' ],
} )
export class FooterComponent {

    constructor() {
    }

}
