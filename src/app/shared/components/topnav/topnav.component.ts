import { select } from '@angular-redux/store';
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component( {
    selector: 'mtjr-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: [ './topnav.component.min.css' ],
} )
export class TopnavComponent {

    @Input() section: string;
    @select( [ 'firebase', 'data' ] ) readonly firebaseData$: Observable<any>;
    @select( [ 'app', 'state', 'activeRoute' ] ) readonly activeRoute$: Observable<string>;
    @ViewChild( 'dropContent' ) dropContent: ElementRef;

    constructor() {
    }
}
