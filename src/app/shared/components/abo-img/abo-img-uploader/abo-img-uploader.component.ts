/**
 * Created by bothmena on 19/01/17.
 */

import { Component, Input, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { UserService } from '../../../services/models/user.service';
import { AppGlobals } from '../../../utils/app.globals';
import { ABOImg } from '../abo-img';

@Component( {
    selector   : 'mtjr-img-uploader',
    templateUrl: 'abo-img-uploader.component.html',
    styleUrls  : [ 'abo-img-uploader.component.min.css' ],
} )

export class ABOImgUploaderComponent extends ABOImg implements OnInit {

    @Input() protected entity: string;
    @Input() protected id: number;
    private uploader: FileUploader;

    constructor( private userService: UserService ) {
        super();
    }

    ngOnInit(): void {

        const options: Object       = {
            url            : this.getUploadUrl(),
            method         : 'POST',
            itemAlias      : 'app_image[file]',
            allowedMimeType: [ 'image/jpeg', 'image/png', 'image/gif' ],
            maxFileSize    : 3145728, // 1048576 = 1MB, 3145728 = 3MB
        };
        this.uploader               = new FileUploader( options );
        this.uploader.onCompleteAll = () => {
            // this._modal.closeModal();
            this.userService.updateUserFireData();
        };
        this.init();
    }

    uploadImage() {

        const file: File = ABOImgUploaderComponent.dataURLtoFile( this.data.image, this.filename );
        this.uploader.clearQueue();
        this.uploader.addToQueue( [ file ] );
        this.uploader.uploadAll();
    }

    private getUploadUrl(): string {

        return AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/images/' + this.entity + '/' + this.id
            + '/' + this.type;
    }
}
