/**
 * Created by bothmena on 19/01/17.
 */

import { Component, Output, EventEmitter } from '@angular/core';
import { ABOImg } from '../abo-img';

@Component({
    selector: 'mtjr-img-cropper',
    templateUrl: 'abo-img-cropper.component.html',
    styleUrls: ['abo-img-cropper.component.min.css'],
})

export class ABOImgCropperComponent extends ABOImg {

    @Output() imageCropped: EventEmitter<File> = new EventEmitter();

    constructor() {
        super();
    }

    cropImage() {

        const image: File = ABOImgCropperComponent.dataURLtoFile( this.data.image, this.filename );
        this.imageCropped.emit( image );
        // this._modal.closeModal();
    }
}
