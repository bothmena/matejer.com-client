import { Input, OnInit, ViewChild } from '@angular/core';
import { CropperSettings, ImageCropperComponent } from 'ng2-img-cropper';
/**
 * Created by bothmena on 24/03/17.
 */

export class ABOImg implements OnInit {

    @Input() protected type: string; // prf|cvr|glr
    protected width: number;
    protected height: number;

    protected data: any;
    protected preview             = false;
    protected filename            = '';
    protected cropperSettings: CropperSettings;
    @ViewChild( 'cropper', undefined ) protected cropper: ImageCropperComponent;
    protected hasBaseDropZoneOver = false;

    ngOnInit(): void {

        this.init();
    }

    protected fileChangeListener( $event: any ) {

        this.filename = $event.target.files[ 0 ].name;
        this.readFile( $event.target.files[ 0 ] );
    }

    protected onFileDrop( files: File[] ) {

        this.readFile( files[ 0 ] );
        this.filename = files[ 0 ].name;
        if ( files.length > 1 ) {
            console.log( 'please drop one file, extra files would be ignored.' );
        }
    }

    protected readFile( file: File ) {

        const image: any           = new Image();
        const myReader: FileReader = new FileReader();
        const that                 = this;
        myReader.onloadend         = function ( loadEvent: any ) {
            image.src = loadEvent.target.result;
            that.cropper.setImage( image );
        };
        myReader.readAsDataURL( file );
    }

    static dataURLtoFile( dataurl: any, filename: string ): File {
        let arr                                             = dataurl.split( ',' ), mime = arr[ 0 ].match( /:(.*?);/ )[ 1 ],
            bstr = atob( arr[ 1 ] ), n = bstr.length, u8arr = new Uint8Array( n );
        while ( n-- ) {
            u8arr[ n ] = bstr.charCodeAt( n );
        }
        return new File( [ u8arr ], filename, { type: mime } );
    }

    protected init() {

        let width: number;
        let height: number;
        switch ( this.type ) {
            case 'prf':
                width  = 500;
                height = 500;
                break;
            case 'cvr':
                width  = 1200;
                height = 400;
                break;
        }

        this.cropperSettings = new CropperSettings();

        this.cropperSettings.croppedWidth  = width;
        this.cropperSettings.croppedHeight = height;
        this.cropperSettings.width         = width;
        this.cropperSettings.height        = height;
        this.cropperSettings.minWidth      = width;
        this.cropperSettings.minHeight     = height;

        this.cropperSettings.canvasWidth  = 700;
        this.cropperSettings.canvasHeight = 400;

        this.cropperSettings.preserveSize = false;
        this.cropperSettings.keepAspect   = true;
        this.cropperSettings.noFileInput  = true;
        this.cropperSettings.rounded      = width === height;
        this.data                         = {};
        /**
         this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
         this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
         this.cropperSettings.keepAspect = false;
         this.cropperSettings.rounded = true;
         */
    }

    protected fileOverBase( e: any ) {
        this.hasBaseDropZoneOver = e;
    }
}
