import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { ABONavigatorService } from '../services/abo-navigator.service';

@Directive( {
    selector: '[mtjrNavigate]',
} )
export class ABONavigateDirective implements OnInit {

    /**
     * mtjrNavigate: the route name
     */
    @Input() mtjrNavigate: string;
    @Input() paramOne = '';
    @Input() paramTwo = '';

    constructor( private navigator: ABONavigatorService, private el: ElementRef ) {
    }

    ngOnInit(): void {

        this.el.nativeElement.style.cursor = 'pointer';
    }

    @HostListener( 'click' ) onClick() {

        if ( this.paramOne === '' ) {

            this.navigator.goto( this.mtjrNavigate );
        } else if ( this.paramTwo === '' ) {

            this.navigator.goto( this.mtjrNavigate, this.paramOne );
        } else {

            this.navigator.goto( this.mtjrNavigate, this.paramOne, this.paramTwo );
        }

    }
}
