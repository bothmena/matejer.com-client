/**
 * Created by bothmena on 12/06/17.
 */

import { AppAction } from 'app/shared/redux/models/app-action';

export class FirebaseActions {

    static FIREBASE_FETCH = 'FIREBASE_FETCH';
    static FIREBASE_EMPTY = 'FIREBASE_EMPTY';

    static firebaseFetch( data ): AppAction {

        return {
            type   : FirebaseActions.FIREBASE_FETCH,
            payload: data,
        };
    }

    static firebaseEmpty(): AppAction {

        return <AppAction>{ type: FirebaseActions.FIREBASE_EMPTY };
    }
}
