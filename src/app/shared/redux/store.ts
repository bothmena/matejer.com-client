/**
 * Created by bothmena on 12/06/17.
 */

import { combineReducers } from 'redux';
import { appReducer } from './app/app.store';
import { firebaseReducer } from './firebase/firebase.store';
import { State } from './models/state';

export const rootReducer = combineReducers<State>( {

    app     : appReducer,
    firebase: firebaseReducer,
} );
