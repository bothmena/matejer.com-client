/**
 * Created by bothmena on 12/06/17.
 */

import { AppState, INIT_APP_STATE } from './app-state';
import { FirebaseState, INIT_FIREBASE_STATE } from './firebase-state';

export interface State {

    app: AppState;
    firebase: FirebaseState;
}

export const INIT_STATE = {

    app     : INIT_APP_STATE,
    firebase: INIT_FIREBASE_STATE,
};
