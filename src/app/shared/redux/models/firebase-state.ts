/**
 * Created by bothmena on 12/06/17.
 */

export interface FireData {

    id: number;
    username: string;
    email: string;
    first_name: string;
    middle_name: string;
    last_name: string;
    birthday: string;
    gender: string;
    cin: string;
    language: string;
    role: string;
    image_url: string;
    institute_id: number;
    institute_slug: string;
    location: {
        id: number;
        country: string;
        state: string;
        city: string;
        address: string;
        zip_code: string;
        is_primary: boolean;

    };
}

export interface FirebaseState {

    data: FireData;
}

export const INIT_FIREBASE_STATE: FirebaseState = {

    data: {
        id: 0,
        username: '',
        email: '',
        first_name: '',
        middle_name: '',
        last_name: '',
        birthday: '',
        gender: '',
        cin: '',
        language: '',
        role: '',
        image_url: '',
        institute_id: 0,
        institute_slug: '',
        location: null,
    }
};
