/**
 * Created by bothmena on 12/06/17.
 */

export class AppAction {

    type: string;
    payload?: any;

    constructor( type: string, payload: any = {} ) {
        this.type    = type;
        this.payload = payload;
    }
}
