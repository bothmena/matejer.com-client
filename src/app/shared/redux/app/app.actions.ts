/**
 * Created by bothmena on 12/06/17.
 */

import { AppAction } from 'app/shared/redux/models/app-action';

export class AppActions {

    static SP_WEBSITE_ADD = 'SP_WEBSITE_ADD';
    static SP_WEBSITE_REMOVE = 'SP_WEBSITE_REMOVE';
    static ACTIVE_ROUTE_CHANGED = 'ACTIVE_ROUTE_CHANGED';

    static spWebsiteAdd( website: string ): AppAction {

        return { type: AppActions.SP_WEBSITE_ADD, payload: website };
    }

    static spWebsiteRemove( website: string ): AppAction {

        return { type: AppActions.SP_WEBSITE_REMOVE, payload: website };
    }

    static activeRouteChanged( routeName: string ): AppAction {

        return { type: AppActions.ACTIVE_ROUTE_CHANGED, payload: routeName };
    }
}
