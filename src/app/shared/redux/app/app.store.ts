import { AppAction } from '../models/app-action';
import { AppState, INIT_APP_STATE } from '../models/app-state';
import { AppReducer } from './app-reducer';
import { AppActions } from './app.actions';
import { tassign } from 'tassign';
/**
 * Created by bothmena on 12/06/17.
 */

export function appReducer( lastState: AppState = INIT_APP_STATE, action: AppAction ): AppState {

    const appRed: AppReducer = new AppReducer( lastState, action );

    switch ( action.type ) {

        case AppActions.SP_WEBSITE_ADD:
            return appRed.addSPWebsites();

        case AppActions.SP_WEBSITE_REMOVE:
            return appRed.removeSPWebsites();

        case AppActions.ACTIVE_ROUTE_CHANGED:
            return tassign( lastState, {state: {
                loading: lastState.state.loading,
                activeRoute: action.payload
            }} );
    }

    // We don't care about-us any other actions right now.
    return lastState;
}
