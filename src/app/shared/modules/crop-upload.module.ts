/**
 * Created by bothmena on 19/01/17.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FileUploadModule } from 'ng2-file-upload';
import { ImageCropperModule } from 'ng2-img-cropper';
import { ABOImgCropperComponent } from '../components/abo-img/abo-img-cropper/abo-img-cropper.component';
import { ABOImgUploaderComponent } from '../components/abo-img/abo-img-uploader/abo-img-uploader.component';

@NgModule( {
    imports: [
        CommonModule,
        FileUploadModule,
        ImageCropperModule,
    ],
    declarations: [
        ABOImgCropperComponent,
        ABOImgUploaderComponent,
    ],
    exports: [
        ABOImgCropperComponent,
        ABOImgUploaderComponent,
        FileUploadModule,
        ImageCropperModule,
    ],
} )

export class CropUploadModule {
}
