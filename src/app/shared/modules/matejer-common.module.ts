/**
 * Created by bothmena on 19/01/17.
 */

import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FooterComponent } from '../components/footer/footer.component';
import { TopnavComponent } from '../components/topnav/topnav.component';
import { ValidationMessageDirective } from '../directives/validation-message.directive';
import { ABONavigateDirective } from '../directives/abo-navigate.directive';
import { LocationPipe } from '../pipes/location.pipe';
import { ObjectKeysPipe } from '../pipes/object-keys.pipe';
import { PhoneNumberPipe } from '../pipes/phone-number.pipe';
import { TrustResourcePipe } from '../pipes/trust-resource.pipe';
import { firebaseConfig } from '../utils/firebase-config';
import { CropUploadModule } from './crop-upload.module';

@NgModule( {
    imports: [
        CommonModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        MDBBootstrapModule,
        AngularFireModule.initializeApp( firebaseConfig ),
        AngularFireDatabaseModule, // imports firebase/database, only needed for database features
        AngularFireAuthModule, // imports firebase/auth, only needed for auth features
        CropUploadModule,
    ],
    declarations: [
        TopnavComponent,
        FooterComponent,
        ABONavigateDirective,
        ValidationMessageDirective,
        LocationPipe,
        PhoneNumberPipe,
        TrustResourcePipe,
        ObjectKeysPipe,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CropUploadModule,
        AngularFireModule,
        AngularFireAuthModule,
        TopnavComponent,
        FooterComponent,
        AngularFireDatabaseModule,
        ABONavigateDirective,
        ValidationMessageDirective,
        LocationPipe,
        PhoneNumberPipe,
        TrustResourcePipe,
        ObjectKeysPipe,
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
} )
export class MatejerCommonModule {
}
