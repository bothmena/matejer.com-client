/**
 * Created by bothmena on 22/01/17.
 */

export class JWTToken {

    private _token: string;
    private _refreshToken: string;

    constructor( token: string = '', refreshToken: string = '' ) {

        this.token        = token;
        this.refreshToken = refreshToken;
    }

    get token(): string {
        return this._token;
    }

    set token( value: string ) {
        this._token = value;
    }

    get refreshToken(): string {
        return this._refreshToken;
    }

    set refreshToken( value: string ) {
        this._refreshToken = value;
    }
}
