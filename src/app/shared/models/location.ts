/**
 * Created by bothmena on 22/06/17.
 */

export class Location {

    private _id: number;
    private _country: string;
    private _state: string;
    private _city: string;
    private _address: string;
    private _zipCode: string;
    private _mapLat: number;
    private _mapLong: number;
    private _isPrimary: boolean;

    static build( object: Object ): Location {

        const location = new Location();

        location.id      = object.hasOwnProperty( 'id' ) ? object[ 'id' ] : 0;
        location.country = object.hasOwnProperty( 'country' ) ? object[ 'country' ] : '';
        location.state   = object.hasOwnProperty( 'state' ) ? object[ 'state' ] : '';
        location.city    = object.hasOwnProperty( 'city' ) ? object[ 'city' ] : '';
        location.address = object.hasOwnProperty( 'address' ) ? object[ 'address' ] : '';
        location.zipCode = object.hasOwnProperty( 'zip_code' ) ? object[ 'zip_code' ] : '';
        location.mapLat  = object.hasOwnProperty( 'map_lat' ) ? +object[ 'map_lat' ] : 0;
        location.mapLong  = object.hasOwnProperty( 'map_long' ) ? +object[ 'map_long' ] : 0;
        location.isPrimary  = object.hasOwnProperty( 'is_primary' ) ? object[ 'is_primary' ] : false;

        return location;
    }

    constructor( country = '', state = '', city = '', zipCode = '', address = '', isPrimary = false ) {

        this._country = country;
        this._state = state;
        this._city = city;
        this._zipCode = zipCode;
        this._address = address;
        this._isPrimary = isPrimary;
        this._mapLat = 0;
        this._mapLong = 0;
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get country(): string {
        return this._country;
    }

    set country( value: string ) {
        this._country = value;
    }

    get state(): string {
        return this._state;
    }

    set state( value: string ) {
        this._state = value;
    }

    get city(): string {
        return this._city;
    }

    set city( value: string ) {
        this._city = value;
    }

    get address(): string {
        return this._address;
    }

    set address( value: string ) {
        this._address = value;
    }

    get zipCode(): string {
        return this._zipCode;
    }

    set zipCode( value: string ) {
        this._zipCode = value;
    }

    get mapLat(): number {
        return this._mapLat;
    }

    set mapLat( value: number ) {
        this._mapLat = value;
    }

    get mapLong(): number {
        return this._mapLong;
    }

    set mapLong( value: number ) {
        this._mapLong = value;
    }

    get isPrimary(): boolean {
        return this._isPrimary;
    }

    set isPrimary( value: boolean ) {
        this._isPrimary = value;
    }
}
