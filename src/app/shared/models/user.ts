import { Location } from './location';
/**
 * Created by bothmena on 22/06/17.
 */

export class User {

    private _id: number;
    private _email: string;
    private _username: string;
    private _firstName: string;
    private _middleName: string;
    private _lastName: string;
    private _birthday: string;
    private _gender: string;
    private _cin: string;
    private _language: string;
    private _location: Location;
    private _role: string;
    private _groups: string[];

    static build( object: Object ): User {

        const user = new User();

        if ( object != null ) {
            user.id = object.hasOwnProperty( 'id' ) ? +object[ 'id' ] : 0;
            user.email = object.hasOwnProperty( 'email' ) ? object[ 'email' ] : '';
            user.username = object.hasOwnProperty( 'username' ) ? object[ 'username' ] : '';
            user.firstName = object.hasOwnProperty( 'first_name' ) ? object[ 'first_name' ] : '';
            user.middleName = object.hasOwnProperty( 'middle_name' ) ? object[ 'middle_name' ] : '';
            user.lastName = object.hasOwnProperty( 'last_name' ) ? object[ 'last_name' ] : '';
            user.birthday = object.hasOwnProperty( 'birthday' ) ? object[ 'birthday' ] : '';
            user.gender = object.hasOwnProperty( 'gender' ) ? object[ 'gender' ] : '';
            user.cin = object.hasOwnProperty( 'cin' ) ? object[ 'cin' ] : '';
            user.language = object.hasOwnProperty( 'language' ) ? object[ 'language' ] : '';
            user.role = object.hasOwnProperty( 'role' ) ? object[ 'role' ] : '';
            user.groups = object.hasOwnProperty( 'groups' ) ? object[ 'groups' ] : [];

            if ( object.hasOwnProperty( 'location' ) && object[ 'location' ] ) {
                user.location = Location.build( object[ 'location' ] );
            } else {
                user.location = new Location();
            }
        }
        return user;
    }

    constructor() {

        this.location = new Location();
    }

    name(): string {

        let name = '';
        if ( this.firstName ) {
            name += this.firstName + ' ';
        }
        if ( this.lastName ) {
            name += this.lastName;
        }
        return name;
    }

    fullName(): string {

        let name = '';
        if ( this.firstName ) {
            name += this.firstName + ' ';
        }
        if ( this.middleName ) {
            name += this.middleName + ' ';
        }
        if ( this.lastName ) {
            name += this.lastName;
        }
        return name;
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get email(): string {
        return this._email;
    }

    set email( value: string ) {
        this._email = value;
    }

    get username(): string {
        return this._username;
    }

    set username( value: string ) {
        this._username = value;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName( value: string ) {
        this._firstName = value;
    }

    get middleName(): string {
        return this._middleName;
    }

    set middleName( value: string ) {
        this._middleName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName( value: string ) {
        this._lastName = value;
    }

    get birthday(): string {
        return this._birthday;
    }

    set birthday( value: string ) {
        this._birthday = value;
    }

    get gender(): string {
        return this._gender;
    }

    set gender( value: string ) {
        this._gender = value;
    }

    get cin(): string {
        return this._cin;
    }

    set cin( value: string ) {
        this._cin = value;
    }

    get language(): string {
        return this._language;
    }

    set language( value: string ) {
        this._language = value;
    }

    get location(): Location {
        return this._location;
    }

    set location( value: Location ) {
        this._location = value;
    }

    get role(): string {
        return this._role;
    }

    set role( value: string ) {
        this._role = value;
    }

    get groups(): string[] {
        return this._groups;
    }

    set groups( value: string[] ) {
        this._groups = value;
    }
}
