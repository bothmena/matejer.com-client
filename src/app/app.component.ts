import { Component } from '@angular/core';

@Component({
  selector: 'mtjr-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

    /**
     * @todo Dispatch a redux action to initialize app.state.activeRoute with the current route @OnInit.
     */
}
