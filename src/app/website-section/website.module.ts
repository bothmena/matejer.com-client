import { NgModule } from '@angular/core';
import { MatejerCommonModule } from '../shared/modules/matejer-common.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WebsiteRoutingModule } from './website-routing.module';
import { WebsiteComponent } from './website/website.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TranslateModule } from '@ngx-translate/core';
import { AddEntityComponent } from './add-entity/add-entity.component';
import { CropUploadModule } from '../shared/modules/crop-upload.module';
import { BrowseComponent } from './browse/browse.component';
import { BrowseProductsComponent } from './browse-products/browse-products.component';
import { BrowseShopsComponent } from './browse-shops/browse-shops.component';

@NgModule( {
    imports     : [
        MatejerCommonModule,
        TranslateModule,
        CropUploadModule,
        WebsiteRoutingModule,
    ],
    declarations: [
        WebsiteComponent,
        HomeComponent,
        AboutUsComponent,
        PageNotFoundComponent,
        ContactUsComponent,
        AddEntityComponent,
        BrowseComponent,
        BrowseProductsComponent,
        BrowseShopsComponent,
    ],
    providers   : [],
} )

export class WebsiteModule {
}
