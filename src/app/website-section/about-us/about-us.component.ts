import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component( {
    templateUrl: './about-us.component.html',
    styleUrls: [ './about-us.component.min.css' ],
} )
export class AboutUsComponent implements OnInit {

    constructor( private meta: Meta, private title: Title ) {
    }

    ngOnInit() {

        this.title.setTitle( 'My spiffy about-us page' );
        this.meta.addTags( [
            { name: 'author', content: 'Aymen Ben Othmen' },
            { name: 'keywords', content: 'angular 4 about-us, about-us seo' },
            { name: 'description', content: 'This is my about-us page description!!' },
        ] );
    }
}
