import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { AddEntityComponent } from './add-entity/add-entity.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomeComponent } from './home/home.component';
import { WebsiteComponent } from './website/website.component';
import { BrowseComponent } from './browse/browse.component';
import { BrowseProductsComponent } from './browse-products/browse-products.component';
import { BrowseShopsComponent } from './browse-shops/browse-shops.component';

const websiteRoutes: Routes = [
    {
        path: '', component: WebsiteComponent, children: [
        { path: '', component: HomeComponent },                                 // w_hm
        { path: 'about-us', component: AboutUsComponent },                      // w_abt
        { path: 'contact-us', component: ContactUsComponent },                  // w_ctu
        { path: 'add-entity', component: AddEntityComponent },                  // w_ade
        {
            path: 'browse', component: BrowseComponent, children: [
            { path: '', redirectTo: 'products', pathMatch: 'full' },               // w_brw
            { path: 'products', component: BrowseProductsComponent },           // w_brp
            { path: 'shops', component: BrowseShopsComponent },                 // w_brs
        ],
        },
    ],
    },
];

@NgModule( {
    imports: [ RouterModule.forChild( websiteRoutes ) ],
    exports: [ RouterModule ],
} )
export class WebsiteRoutingModule {
}
