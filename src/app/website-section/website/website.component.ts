import { Component } from '@angular/core';

@Component( {
    template: `
        <mtjr-topnav section="website"></mtjr-topnav>
        <main>
            <div class="container" style="height:1300px;">
                <router-outlet></router-outlet>
            </div>
        </main>
        <mtjr-footer></mtjr-footer>
    `,
} )
export class WebsiteComponent {

    constructor() {
    }

}
