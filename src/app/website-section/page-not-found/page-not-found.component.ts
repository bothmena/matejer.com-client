import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component( {
    templateUrl: './page-not-found.component.html',
    styleUrls: [ './page-not-found.component.min.css' ],
} )
export class PageNotFoundComponent implements OnInit {

    constructor( private meta: Meta, private title: Title ) {
    }

    ngOnInit() {

        this.title.setTitle( 'Content Not Found!' );
        this.meta.addTags( [
            { name: 'author', content: 'Aymen Ben Othmen' },
            { name: 'keywords', content: 'angular 4 about-us, about-us seo' },
            { name: 'description', content: 'This is my about-us page description!!' },
        ] );
    }

}
