/**
 * Created by bothmena on 13/02/17.
 */
import {Component, ViewChild} from '@angular/core';
import {CropperSettings, ImageCropperComponent} from 'ng2-img-cropper';
import {FileUploader} from 'ng2-file-upload';

// const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
const URL = 'http://localhost:8000/web/upload/images';

@Component({
    templateUrl: 'add-entity.component.html',
    styleUrls: ['add-entity.component.min.css'],
})
export class AddEntityComponent {

    data: any;
    counter = 0;
    cropperSettings: CropperSettings;
    // ng2-file-upload ->
    options: Object = {
        url: URL,
        method: 'POST',
        itemAlias: 'app_image[file]',
        allowedMimeType: ['image/jpeg', 'image/png'],
        maxFileSize: 3145728, // 1024 = 1kB, 1048576 = 1MB, 3145728 = 3MB
    };
    public uploader: FileUploader = new FileUploader( this.options );
    public hasBaseDropZoneOver = false;
    public hasAnotherDropZoneOver = false;
    // <- ng2-file-upload

    @ViewChild('cropper', undefined) cropper: ImageCropperComponent;

    constructor() {

        this.init();
    }

    fileChangeListener($event: any) {

        const image: any = new Image();
        const file: File = $event.target.files[0];
        const myReader: FileReader = new FileReader();
        const that = this;
        myReader.onloadend = function (loadEvent: any) {
            image.src = loadEvent.target.result;
            that.cropper.setImage(image);
        };

        myReader.readAsDataURL(file);
    }

    uploadImage() {
        // console.log("image cropped: " + this.data.image );
        this.counter++;
        console.log( 'size: ' + this.data.image.length + ', #: ' + this.counter );

        const file: File = this.dataURLtoFile( this.data.image, 'a.jpg' );
        console.log( file );

        this.uploader.clearQueue();
        this.uploader.addToQueue( [file] );
    }

    dataURLtoFile(dataurl: any, filename: string): File {
        let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type: mime});
    }

    private init() {

        this.cropperSettings = new CropperSettings();

        this.cropperSettings.canvasWidth = 500;
        this.cropperSettings.canvasHeight = 250;
        this.cropperSettings.width = 1500;
        this.cropperSettings.height = 750;
        this.cropperSettings.minWidth = 1200;
        this.cropperSettings.minHeight = 600;
        this.cropperSettings.croppedWidth = 1000;
        this.cropperSettings.croppedHeight = 500;

        this.cropperSettings.preserveSize = false;
        this.cropperSettings.keepAspect = true;
        this.cropperSettings.noFileInput = true;
        this.data = {};
        /**
         this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
         this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
         this.cropperSettings.keepAspect = false;
         this.cropperSettings.rounded = true;
         */
    }

    // ng2-file-upload
    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    // ng2-file-upload
    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }
}
