import { Component, OnInit } from '@angular/core';
import { ABOTranslationLoaderService } from '../../shared/services/abo-translation-loader.service';

@Component( {
    template: `
        <mtjr-topnav section="admin"></mtjr-topnav>
        <div class="container-fluid">
            <router-outlet></router-outlet>
        </div>
        <mtjr-footer></mtjr-footer>

    `,
} )
export class AdminComponent implements OnInit {

    constructor( private translationLoader: ABOTranslationLoaderService ) {
    }

    ngOnInit() {

        this.translationLoader.loadTranslations( 'admin' );
    }
}
