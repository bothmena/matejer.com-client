import { NgModule } from '@angular/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { AccountModule } from '../shared/modules/account.module';
// import { ZdlmCommonModule } from '../shared/modules/zdlm-common.module';
// import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';

import { AdminRoutingModule } from './admin-routing.module';
// import { AdminComponent } from './admin/admin.component';
// import { InsAdminsComponent } from './ins-admins/ins-admins.component';
// import { InstitutesComponent } from './institutes/institutes.component';

@NgModule( {
    imports: [
        // ZdlmCommonModule,
        // AccountModule,
        // TranslateModule,
        AdminRoutingModule,
    ],
    declarations: [
        // InstitutesComponent,
        // AdminDashboardComponent,
        // AdminComponent,
        // InsAdminsComponent,
    ],
    providers: [],
} )
export class AdminModule {
}
