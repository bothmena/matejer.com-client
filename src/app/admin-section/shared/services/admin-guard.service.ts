import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route,
    RouterStateSnapshot,
} from '@angular/router';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import { JWTTokenService } from '../../../shared/services/models/jwt-token.service';
/**
 * Created by bothmena on 12/02/17.
 */

@Injectable()
export class AdminGuardService implements CanActivate, CanActivateChild, CanLoad {

    constructor( private jwtToken: JWTTokenService, private auth: AuthenticationService ) {
    }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {

        return this.activate();
    }

    canActivateChild( childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {

        return this.activate();
    }

    canLoad( route: Route ): boolean {

        return this.jwtToken.role === 'admin';
    }

    private activate(): boolean {

        if ( this.jwtToken.role === 'admin' ) {

            return true;
        }
        this.auth.goToProfile();
        return false;
    }
}
