import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
/*import { AdminGuardService } from 'app/admin-section/shared/services/admin-guard.service';
import { AccountInfoEditComponent } from '../shared/components/account/account-info-edit/account-info-edit.component';

import { AccountInfoComponent } from '../shared/components/account/account-info/account-info.component';
import { ContactEditComponent } from '../shared/components/account/contact-edit/contact-edit.component';
import { ContactComponent } from '../shared/components/account/contact/contact.component';
import { CredentialsEditComponent } from '../shared/components/account/credentials-edit/credentials-edit.component';
import { LocationMapEditComponent } from '../shared/components/account/location-map-edit/location-map-edit.component';
import { LocationMapComponent } from '../shared/components/account/location-map/location-map.component';
import { ProfileComponent } from '../shared/components/account/profile/profile.component';
import { SettingsComponent } from '../shared/components/account/settings/settings.component';
import { SocialProfilesEditComponent } from '../shared/components/account/social-profiles-edit/social-profiles-edit.component';
import { SocialProfilesComponent } from '../shared/components/account/social-profiles/social-profiles.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { InsAdminsComponent } from './ins-admins/ins-admins.component';
import { InstitutesComponent } from './institutes/institutes.component';
import { CredentialsComponent } from '../shared/components/account/credentials/credentials.component';
import { WebsiteEditComponent } from '../shared/components/account/website-edit/website-edit.component';*/

const routes: Routes = [
    /*{
        path            : '',
        component       : AdminComponent,
        canActivate     : [ AdminGuardService ],
        canActivateChild: [ AdminGuardService ],
        children        : [
            {
                path: '', component: ProfileComponent, children: [
                { path: '', redirectTo: 'account-info', pathMatch: 'full' },
                { path: 'account-info', component: AccountInfoComponent },              //  a_pr_aci
                { path: 'location', component: LocationMapComponent },                  //  a_pr_lam
                { path: 'contact-info', component: ContactComponent },                  //  a_pr_cti
                { path: 'social-profiles', component: SocialProfilesComponent },        //  a_pr_spr
                { path: 'credentials', component: CredentialsComponent },               //  a_pr_crd
            ],
            },
            {
                path: 'settings', component: SettingsComponent, children: [
                { path: '', redirectTo: 'account-info', pathMatch: 'full' },
                { path: 'account-info', component: AccountInfoEditComponent },          //  a_st_aci
                { path: 'location', component: LocationMapEditComponent },              //  a_st_lam
                { path: 'contact-info', component: ContactEditComponent },              //  a_st_cti
                { path: 'social-profiles', component: SocialProfilesEditComponent },    //  a_st_spr
                { path: 'credentials', component: CredentialsEditComponent },           //  a_st_crd
                { path: 'website-preferences', component: WebsiteEditComponent },       //  a_st_wpr
            ],
            },
            { path: 'dashboard', component: AdminDashboardComponent },                      // a_dsh
            { path: 'institutes', component: InstitutesComponent },                         // a_ins
            { path: 'ins-admins', component: InsAdminsComponent },                          // a_ina
        ],
    },*/
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ],
} )
export class AdminRoutingModule {
}
