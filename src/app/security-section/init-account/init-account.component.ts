import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs/Subscription';
import * as zxcvbn from 'zxcvbn';
import { ABONavigatorService } from '../../shared/services/abo-navigator.service';
import { UserService } from '../../shared/services/models/user.service';
import { ABOValidators } from '../../shared/utils/abo.validators';
import { tnCityState } from '../../shared/utils/util-functions';

declare var $: any;

@Component( {
    templateUrl: './init-account.component.html',
    styleUrls: [ './init-account.component.min.css' ],
} )
export class InitAccountComponent implements OnInit, OnDestroy {

    initForm: FormGroup;
    formErrors: any = {};
    isLoading = false;
    viewPass = false;
    @ViewChild( 'tabs' ) tabs: ElementRef;
    cities: string[] = [];
    stateCities = tnCityState;
    passClass = 'determinate';
    private token: string;
    private paramSub: Subscription;

    constructor( private fb: FormBuilder,
        private route: ActivatedRoute,
        private translate: TranslateService,
        private navigator: ABONavigatorService,
        private fauth: AngularFireAuth,
        private userService: UserService ) {
    }

    ngOnInit(): void {
        this.buildForm();
        this.paramSub = this.route.params
            .subscribe( ( params: Params ) => this.token = params[ 'token' ] );
    }

    ngOnDestroy(): void {

        this.paramSub.unsubscribe();
    }

    passStrengthFn() {

        switch ( zxcvbn( this.initForm.get( 'plainPassword' ).get( 'password' ).value ).score ) {

            case 0:
                this.passClass = 'determinate score-20';
                break;
            case 1:
                this.passClass = 'determinate score-40';
                break;
            case 2:
                this.passClass = 'determinate score-60';
                break;
            case 3:
                this.passClass = 'determinate score-80';
                break;
            case 4:
                this.passClass = 'determinate score-100';
                break;

        }
    }

    toSlide( tabId: string ) {

        $( this.tabs.nativeElement ).tabs( 'select_tab', tabId );
    }

    levelOneValid(): boolean {

        return this.initForm.get( 'username' ).valid && this.initForm.get( 'plainPassword' ).valid;
    }

    levelTwoValid(): boolean {

        return this.initForm.get( 'firstName' ).valid && this.initForm.get( 'lastName' ).valid &&
            this.initForm.get( 'gender' ).valid && this.initForm.get( 'language' ).valid &&
            this.initForm.get( 'birthday' ).valid && this.initForm.get( 'cin' ).valid;
    }

    buildForm(): void {

        const password = new FormControl( '', Validators.compose( [
            Validators.required,
            Validators.minLength( 8 ),
            Validators.maxLength( 51 ),
            Validators.pattern( /^(?=\S*\d)(?=\S*[a-z])(?=\S*[A-Z])\S*$/ ),
        ] ) );
        const confirmPassword = new FormControl( '', CustomValidators.equalTo( password ) );

        this.initForm = this.fb.group( {
            'username': [ '', Validators.compose( [
                Validators.required,
                Validators.minLength( 3 ),
                Validators.maxLength( 31 ),
                Validators.pattern( /^[a-zA-Z]{1}[\w.-]+$/ ),
            ] ) ],
            'plainPassword': this.fb.group( {
                'password': password,
                'confirm_password': confirmPassword,
            } ),

            'firstName': [ '', Validators.compose( [
                Validators.required,
                Validators.minLength( 3 ),
                Validators.maxLength( 31 ),
            ] ) ],
            'lastName': [ '', Validators.compose( [
                Validators.required,
                Validators.minLength( 3 ),
                Validators.maxLength( 31 ),
            ] ) ],
            'gender': [ '', Validators.compose( [
                Validators.required,
                ABOValidators.inArray( [ 'male', 'female' ] ),
            ] ) ],
            'language': [ 'fr', Validators.compose( [
                Validators.required,
                ABOValidators.inArray( [ 'ar', 'en', 'fr' ] ),
            ] ) ],
            'birthday': [ '', Validators.compose( [
                Validators.required,
                ABOValidators.dateLessThan( new Date( new Date().setFullYear( new Date().getUTCFullYear() - 3 ) ) ),
                ABOValidators.dateGreaterThan( new Date( new Date().setFullYear( new Date().getUTCFullYear() - 100 ) ) ),
            ] ) ],
            'cin': [ '', Validators.compose( [
                Validators.required,
                Validators.pattern( /^\d+$/ ),
                Validators.minLength( 8 ),
                Validators.maxLength( 8 ),
            ] ) ],

            'location': this.fb.group( {
                'country': [ 'TN' ],
                'state': [ '', Validators.compose( [
                    Validators.required,
                    Validators.minLength( 3 ),
                    Validators.maxLength( 31 ),
                ] ) ],
                'city': [ '', Validators.compose( [
                    Validators.required,
                    Validators.minLength( 3 ),
                    Validators.maxLength( 31 ),
                ] ) ],
                'address': [ '', Validators.compose( [
                    Validators.required,
                    Validators.minLength( 15 ),
                    Validators.maxLength( 101 ),
                ] ) ],
                'zipCode': [ '', Validators.compose( [
                    Validators.required,
                    Validators.pattern( /^\d+$/ ),
                    Validators.minLength( 4 ),
                    Validators.maxLength( 4 ),
                ] ) ],
            } ),
        } );
        this.initForm.valueChanges.subscribe( data =>
            this.formErrors = ABOValidators.onValueChanged( this.initForm, 'user' ) );
    }

    submit() {

        this.isLoading = true;
        this.userService.initUser( this.token, this.initForm.value ).subscribe(
            () => this.onSubmitSuccess(),
            ( error ) => this.onSubmitError( error ),
        );
    }

    onStateChange() {

        const state = this.initForm.get( 'location' ).get( 'state' ).value;
        if ( state === '' ) {

            this.cities = [];
        } else {

            for ( const stt of this.stateCities ) {

                if ( state === stt.name ) {

                    this.cities = stt.cities;
                }
            }
        }
        this.initForm.get( 'location.city' ).setValue( state );
    }

    getFormVal( fName: string, gName: string = null ) {

        if ( gName ) {

            return this.initForm.get( gName ).get( fName ).value;
        }
        return this.initForm.get( fName ).value;
    }

    getPass() {

        const password = this.initForm.get( 'plainPassword' ).get( 'password' ).value;
        if ( password === '' ) {

            return '';
        }
        return this.viewPass ? password : '**********';
    }

    private onSubmitSuccess() {

        const username = this.initForm.get( 'username' ).value;
        const password = this.initForm.get( 'plainPassword' ).get( 'password' ).value;

        console.log( 'creating user in firebase with username: ', username, ' and password: ', password );

        this.fauth.auth.createUserWithEmailAndPassword( username + '@zednilma.com', password )
            .then( () => {

                this.isLoading = false;
                this.navigator.goto( 'c_lgn' );
                this.userService.updateUserFireData();
            } )
            .catch( err => console.log( 'couldn\'t save the user data in firebase!', err ) );

        this.translate.get( 'init_account.submit_suc', {} ).subscribe( res => console.log( res, 5000 ) ); // toast
    }

    private onSubmitError( error: Response | any ) {

        try {

            const jsonError = error.json();
            console.log( jsonError );
            switch ( +jsonError.error.code ) {
                case 404:
                    this.translate.get( 'init_account.404', { token: this.token } )
                        .subscribe( res => console.log( res, 5000 )); // toast
                    break;
                case 500:
                    this.translate.get( 'app.500', {} ).subscribe( res => console.log( res, 5000 ) ); // toast
                    break;
                default:
                    console.log( 'crap! I didn\'t know that this was coming!' ); // toast
                    break;
            }
        } catch ( e ) {
            console.log( e );
        }
        this.isLoading = false;
    }
}
