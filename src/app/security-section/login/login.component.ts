import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JwtHelper } from 'angular2-jwt';
import { AngularFireAuth } from 'angularfire2/auth';
import { JWTToken } from '../../shared/models/jwt-token';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { JWTTokenService } from '../../shared/services/models/jwt-token.service';
import { ABOValidators } from '../../shared/utils/abo.validators';

@Component( {
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.min.css' ],
} )
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    formErrors: any = {};
    isLoading = false;
    private jwtHelper: JwtHelper = new JwtHelper();

    constructor( private fb: FormBuilder,
        private authService: AuthenticationService,
        private jwtTokenService: JWTTokenService,
        private fauth: AngularFireAuth ) {
    }

    ngOnInit(): void {
        this.buildForm();
    }

    buildForm(): void {

        this.loginForm = this.fb.group( {
            'username': [ '', Validators.required ],
            'password': [ '', Validators.required ],
            'remember_me': [ false ],
        } );
        this.loginForm.valueChanges.subscribe( data =>
            this.formErrors = ABOValidators.onValueChanged( this.loginForm, 'user' ) );
    }

    login(): void {

        this.isLoading = true;
        this.authService.authenticate( this.loginForm.value ).subscribe(
            ( jwtToken: JWTToken ) => {

                const username = this.jwtHelper.decodeToken( jwtToken.token ).username;
                this.fauth.auth.signInWithEmailAndPassword( username + '@zednilma.com',
                    this.loginForm.get( 'password' ).value )
                    .then( () => this.authService.onAuthenticateSuccess( jwtToken ) )
                    .catch(
                        ( error: any ) => {

                            console.log( 'an error has occurred while sign in user in firebase.', error );
                        },
                    );
                console.log( 'Welcome Back<br/>We\'re happy to have you ' + this.loginForm.get( 'username' ).value, 5000 ); // toast
                this.isLoading = false;
            },
            error => this.onLoginError( error ),
        );
    }

    private onLoginError( error: any ) {

        this.authService.onAuthenticateFailure( error );
        const regex: RegExp = /(401)+/;
        if ( regex.test( error ) ) {

            console.log( 'Bad credentials Given<br/>Either password or username is incorrect', 5000 ); // toast
        } else {

            console.log( 'An error occured during authentication<br/>Please try again.', 5000 ); // toast
        }
        this.isLoading = false;
        this.loginForm.get( 'username' ).setErrors( { 'badcredentials': {} } );
        this.loginForm.get( 'password' ).setErrors( { 'badcredentials': {} } );
        this.loginForm.markAsDirty();
        this.loginForm.markAsTouched();
    }
}
