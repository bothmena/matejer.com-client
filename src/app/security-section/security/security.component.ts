import { Component, OnInit } from '@angular/core';

@Component( {
    template: `
        <div class="teal lighten-4 loaded page-container sc-outer">
            <div class="container-fluid sc-inner">
                <div class="row">
                    <div id="login-page" class="col s12 push-m3 m6 push-l4 l4">
                        <div class="col s12 z-depth-5 card-panel">
                            <router-outlet></router-outlet>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
} )
export class SecurityComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
    }
}
