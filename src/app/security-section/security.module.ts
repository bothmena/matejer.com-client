import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MatejerCommonModule } from '../shared/modules/matejer-common.module';
import { ABOTranslationLoaderService } from '../shared/services/abo-translation-loader.service';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { InitAccountComponent } from './init-account/init-account.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ResendTokenComponent } from './resend-token/resend-token.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

import { SecurityRoutingModule } from './security-routing.module';
import { SecurityComponent } from './security/security.component';
import { InitAccountGuardService } from './shared/services/init-account-guard.service';
import { LogoutGuardService } from './shared/services/logout-guard.service';

@NgModule( {
    imports: [
        MatejerCommonModule,
        TranslateModule,
        SecurityRoutingModule,
    ],
    declarations: [
        SecurityComponent,
        LoginComponent,
        InitAccountComponent,
        ResetPasswordComponent,
        LogoutComponent,
        ConfirmEmailComponent,
        ResendTokenComponent,
    ],
    providers: [
        LogoutGuardService,
        InitAccountGuardService,
    ],
} )
export class SecurityModule {

    constructor( private aboTranslationLoader: ABOTranslationLoaderService ) {

        this.aboTranslationLoader.loadTranslations( 'shop' );
    }
}
