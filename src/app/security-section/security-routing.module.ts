import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { InitAccountComponent } from './init-account/init-account.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ResendTokenComponent } from './resend-token/resend-token.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SecurityComponent } from './security/security.component';
import { LogoutGuardService } from './shared/services/logout-guard.service';
import { SecurityGuardService } from './shared/services/security-guard.service';
import { InitAccountGuardService } from './shared/services/init-account-guard.service';

const routes: Routes = [
    {
        path            : '',
        component       : SecurityComponent,
        canActivate     : [ SecurityGuardService ],
        canActivateChild: [ SecurityGuardService ],
        children        : [
            { path: 'login', component: LoginComponent },                                   // c_lgn
            { path: 'reset-password', component: ResetPasswordComponent },                  // c_rpw
            { path: 'confirm-email/:token', component: ConfirmEmailComponent },             // c_int
            { path: 'resend-token/:token', component: ResendTokenComponent },               // c_int
        ],
    },
    {
        path   : 'init-account/:token',
        component: InitAccountComponent,
        canActivate: [ InitAccountGuardService ]
    },                                                                                      // c_int
    { path: 'logout', component: LogoutComponent, canActivate: [ LogoutGuardService ] },    // c_lgo
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ],
} )
export class SecurityRoutingModule {
}
