import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { ABONavigatorService } from '../../shared/services/abo-navigator.service';
import { NgRedux } from '@angular-redux/store';
import { State } from '../../shared/redux/models/state';
import { FirebaseActions } from '../../shared/redux/firebase/firebase.actions';

@Component( {
    template: '<p>logging out...</p>',
} )
export class LogoutComponent implements OnInit {

    constructor(
        private authService: AuthenticationService,
        private redux: NgRedux<State>,
        private navigator: ABONavigatorService ) {
    }

    ngOnInit() {

        this.authService.logout()
            .then( () => {

                this.redux.dispatch( FirebaseActions.firebaseEmpty() );
                this.navigator.goto('w_hm');
            } )
            .catch(
                error => console.log( 'firebase signout error', error ),
            );
    }
}
