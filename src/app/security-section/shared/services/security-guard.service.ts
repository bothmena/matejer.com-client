import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route,
    RouterStateSnapshot,
} from '@angular/router';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import { JWTTokenService } from '../../../shared/services/models/jwt-token.service';

@Injectable()
export class SecurityGuardService implements CanActivate, CanActivateChild, CanLoad {

    constructor( private jwtToken: JWTTokenService, private authService: AuthenticationService ) {
    }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {

        return this.activate();
    }

    canActivateChild( childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {

        return this.activate();
    }

    canLoad( route: Route ): boolean {

        console.log( this.jwtToken.role );
        console.log( 'can load shop?', this.jwtToken.role === '' || this.jwtToken.role === null );
        return ( this.jwtToken.role === '' || this.jwtToken.role === null );
    }

    private activate(): boolean {

        if ( !this.authService.isLoggedIn() ) {

            return true;
        }
        this.authService.goToProfile();
        return false;
    }
}
