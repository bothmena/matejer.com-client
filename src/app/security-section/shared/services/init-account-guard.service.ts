import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../../../shared/services/models/user.service';

@Injectable()
export class InitAccountGuardService implements CanActivate {

    constructor( private userService: UserService ) {
    }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> {

        const token = route.params[ 'token' ];
        return this.userService.getUserExistByToken( token, false );
    }
}
