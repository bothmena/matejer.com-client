import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { ABONavigatorService } from '../../../shared/services/abo-navigator.service';
import { JWTTokenService } from '../../../shared/services/models/jwt-token.service';

@Injectable()
export class LogoutGuardService implements CanActivate {

    constructor( private jwtToken: JWTTokenService, private _navigator: ABONavigatorService ) {
    }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {

        if ( this.jwtToken.username !== '' ) {

            return true;
        }
        this._navigator.goto( 'w_hm' );
        return false;
    }
}
