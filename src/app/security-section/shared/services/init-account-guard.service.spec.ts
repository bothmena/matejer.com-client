import { TestBed, inject } from '@angular/core/testing';

import { InitAccountResolverService } from './init-account-resolver.service';

describe('InitAccountGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InitAccountResolverService]
    });
  });

  it('should be created', inject([InitAccountResolverService], (service: InitAccountResolverService) => {
    expect(service).toBeTruthy();
  }));
});
