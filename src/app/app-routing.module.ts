import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './website-section/page-not-found/page-not-found.component';
import { HomeComponent } from './website-section/home/home.component';

const routes: Routes = [
    { path: '', component: HomeComponent, },            // w_hm
    { path: '**', component: PageNotFoundComponent, },
];

@NgModule( {
    imports: [ RouterModule.forRoot( routes ) ],
    exports: [ RouterModule ],
} )

export class AppRoutingModule {
}
